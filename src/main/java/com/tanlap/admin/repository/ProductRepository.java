package com.tanlap.admin.repository;

import com.sun.xml.bind.v2.model.core.ID;
import com.tanlap.admin.security.services.ProductImp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
@Repository
public interface ProductRepository extends JpaRepository<ProductImp, ID> {
    @Query("select new com.tanlap.admin.security.productImp(p.ID, p.Serial" +
            ", p.Name" +
            ", p.Kind," +
            " p.Content," +
            " p.avatar," +
            " p.UNIT_TO_CALCULATE" +
            ", p.Price," +
            " P.Number) from Product p where p.getKind() like ?1) ")
    List<ProductImp> filterByKind( String Kind);

    @Query("select new com.tanlap.admin.security.productImp(p.ID, p.Serial" +
            ", p.Name" +
            ", p.Kind," +
            " p.Content," +
            " p.avatar," +
            " p.UNIT_TO_CALCULATE" +
            ", p.Price," +
            " P.Number) from Product p where p.getName() like ?1)" )
    Optional<ProductImp> findByName(String Name);

    @Query("select new com.tanlap.admin.security.productImp(p.ID, p.Serial" +
            ", p.Name" +
            ", p.Kind," +
            " p.Content," +
            " p.avatar," +
            " p.UNIT_TO_CALCULATE" +
            ", p.Price," +
            " P.Number) from Product p where p.getSerial() like ?1)")
    Optional<ProductImp> findBySerial(String Serial);

  //List<ProductImp> filterByMSD(Date MSD);

    boolean existsBySerial(String Serial);

    boolean existsByNameEquals(String Name);
    @Query("select new com.tanlap.admin.security.productImp(p.ID, p.Serial" +
            ", p.Name" +
            ", p.Kind," +
            " p.Content," +
            " p.avatar," +
            " p.UNIT_TO_CALCULATE" +
            ", p.Price," +
            " P.Number) from Product p")
    List<ProductImp> findAll();
    @Query("select new com.tanlap.admin.security.productImp(p.ID, p.Serial" +
            ", p.Name" +
            ", p.Kind," +
            " p.Content," +
            " p.avatar," +
            " p.UNIT_TO_CALCULATE" +
            ", p.Price," +
            " P.Number) from Product p where p.getPrice()==?1")
    List<ProductImp> findByPrice(double price);




}
