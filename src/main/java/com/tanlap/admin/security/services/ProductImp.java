package com.tanlap.admin.security.services;

import com.tanlap.admin.models.Product;
import com.tanlap.admin.security.services.BaseServices.ProductDTO;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductImp implements ProductDTO {
     @Autowired
    Product product;
    private long ID;
    private String Serial, Name,Title,Kind,Content, Avatar,UNIT_TO_CALCULATE;
    private double Price;
    private int Number;
     public ProductImp(long ID, String serial, String name, String title, String kind, String content, String avatar, String UNIT_TO_CALCULATE, double price, int number) {
         this.ID = ID;
         Serial = serial;
         Name = name;
         Title = title;
         Kind = kind;
         Content = content;
         Avatar = avatar;
         this.UNIT_TO_CALCULATE = UNIT_TO_CALCULATE;
         Price = price;
         Number = number;

     }
    @Override
    public long getID() {
        return product.getID();
    }

    @Override
    public void setID(long ID) {
     product.setID(ID);
    }

    @Override
    public String getSerial() {
        return product.getSerial();
    }

    @Override
    public void setSerial(String serial) {
   product.setSerial(serial);
    }

    @Override
    public String getName() {
        return product.getName();
    }

    @Override
    public void setName(String name) {
    product.setName(name);
    }

    @Override
    public String getTitle() {
        return product.getTitle();
    }

    @Override
    public void setTitle(String title) {
      product.setTitle(title);
    }

    @Override
    public String getKind() {
        return product.getKind();
    }

    @Override
    public void setKind(String kind) {
    product.setKind(kind);
    }

    @Override
    public String getContent() {
        return product.getContent();
    }

    @Override
    public void setContent(String content) {
    product.setContent(content);
    }

    @Override
    public String getAvatar() {
        return product.getAvatar();
    }

    @Override
    public void setAvatar(String avatar) {
    product.setAvatar(avatar);
    }

    @Override
    public String getUNIT_TO_CALCULATE() {
        return product.getUNIT_TO_CALCULATE();
    }

    @Override
    public void setUNIT_TO_CALCULATE(String UNIT_TO_CALCULATE) {
   product.setUNIT_TO_CALCULATE(UNIT_TO_CALCULATE);
    }

    @Override
    public double getPrice() {
        return product.getPrice();
    }

    @Override
    public void setPrice(double price) {
    product.setPrice(price);
    }

    @Override
    public int getNumber() {
        return product.getNumber();
    }

    @Override
    public void setNumber(int number) {
    product.setNumber(number);
    }
}
