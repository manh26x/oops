package com.tanlap.admin.security.services;

import com.tanlap.admin.repository.ProductRepository;
import com.tanlap.admin.security.services.BaseServices.ProductQueryDTO;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class ProductServiceIm implements ProductQueryDTO {
    @Autowired
    ProductRepository productRepository;
    @Override
    public List<ProductImp> filterByKind(String kind) {
        return productRepository.filterByKind(kind);
    }

    @Override
    public Optional<ProductImp> findByName(String Name) {
        return productRepository.findByName(Name);
    }

    @Override
    public List<ProductImp> findByPrice(double price) {
        return productRepository.findByPrice(price);
    }

    @Override
    public List<ProductImp> findAll() {
        return productRepository.findAll();
    }
}
