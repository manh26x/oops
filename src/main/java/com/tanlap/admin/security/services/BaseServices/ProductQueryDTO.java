package com.tanlap.admin.security.services.BaseServices;

import com.tanlap.admin.security.services.ProductImp;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ProductQueryDTO {
   List<ProductImp> filterByKind(String kind);
   Optional<ProductImp> findByName(String Name);
   List<ProductImp> findByPrice(double price);
   List<ProductImp> findAll();
}
