package com.tanlap.admin.security.services.BaseServices;

import org.springframework.stereotype.Service;

import java.io.Serializable;

@Service
public interface ProductDTO extends Serializable {


    public long getID() ;

    public void setID(long ID) ;

    public String getSerial() ;

    public void setSerial(String serial);

    public String getName() ;

    public void setName(String name);

    public String getTitle() ;

    public void setTitle(String title);
    public String getKind() ;
    public void setKind(String kind);

    public String getContent() ;

    public void setContent(String content) ;

    public String getAvatar() ;

    public void setAvatar(String avatar) ;

    public String getUNIT_TO_CALCULATE();

    public void setUNIT_TO_CALCULATE(String UNIT_TO_CALCULATE) ;

    public double getPrice() ;

    public void setPrice(double price) ;

    public int getNumber() ;

    public void setNumber(int number) ;
}
