package com.tanlap.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TanlapApplication {

	public static void main(String[] args) {
		SpringApplication.run(TanlapApplication.class, args);
	}

}
