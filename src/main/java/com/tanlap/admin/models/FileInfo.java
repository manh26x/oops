package com.tanlap.admin.models;

import com.tanlap.admin.security.services.ProductImp;
import org.springframework.beans.factory.annotation.Autowired;

public class FileInfo {
    @Autowired
    ProductImp productImp;
    private String url;
    private String nameFile ;
    public FileInfo(String url, String name) {
        this.url = url;
        nameFile= name;
        productImp.setAvatar(nameFile);
    }

    public String getNameFile() {
        return productImp.getAvatar();
    }

    public void setNameFile(String name) {
        productImp.setAvatar(name);
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
