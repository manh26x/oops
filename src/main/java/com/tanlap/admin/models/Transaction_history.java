package com.tanlap.admin.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Transaction_history {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long ID;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable(name = "product_trading_history",
           joinColumns=  @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name="product_id", referencedColumnName = "id"))
    private long Product_id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable(
            name = "Employee_Session_History",
            joinColumns = @JoinColumn(name = "employee_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id",referencedColumnName = "id")
    )
    private long Employee_id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable(
            name = "user_trading_history",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
    private long Customer_id;
    private int Number;
    private String UNIT_TO_CALCULATE;
    private double Amount_money;

    public Transaction_history(long ID, long product_id, long employee_id, long customer_id, int number, String UNIT_TO_CALCULATE, double amount_money) {
        this.ID = ID;
        Product_id = product_id;
        Employee_id = employee_id;
        Customer_id = customer_id;
        Number = number;
        this.UNIT_TO_CALCULATE = UNIT_TO_CALCULATE;
        Amount_money = amount_money;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public long getProduct_id() {
        return Product_id;
    }

    public void setProduct_id(long product_id) {
        Product_id = product_id;
    }

    public long getEmployee_id() {
        return Employee_id;
    }

    public void setEmployee_id(long employee_id) {
        Employee_id = employee_id;
    }

    public long getCustomer_id() {
        return Customer_id;
    }

    public void setCustomer_id(long customer_id) {
        Customer_id = customer_id;
    }

    public int getNumber() {
        return Number;
    }

    public void setNumber(int number) {
        Number = number;
    }

    public String getUNIT_TO_CALCULATE() {
        return UNIT_TO_CALCULATE;
    }

    public void setUNIT_TO_CALCULATE(String UNIT_TO_CALCULATE) {
        this.UNIT_TO_CALCULATE = UNIT_TO_CALCULATE;
    }

    public double getAmount_money() {
        return Amount_money;
    }

    public void setAmount_money(double amount_money) {
        Amount_money = amount_money;
    }
}
