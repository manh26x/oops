package com.tanlap.admin.models;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Data
@Entity
@Table(name = "products",uniqueConstraints ={
        @UniqueConstraint(columnNames = "serial"),
        @UniqueConstraint(columnNames = "name")
})
public class Product {
    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "History_trading",
            joinColumns = @JoinColumn(name = "product_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "product_id"))
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) @Column(name = "id") private long ID;
    private String Serial, Name,Title,Kind,Content, Avatar,UNIT_TO_CALCULATE;
    private double Price;
    private int Number;

    public Product(long ID, String serial, String name, String title, String kind, String content, String avatar, String UNIT_TO_CALCULATE, double price, int number) {
        this.ID = ID;
        Serial = serial;
        Name = name;
        Title = title;
        Kind = kind;
        Content = content;
        Avatar = avatar;
        this.UNIT_TO_CALCULATE = UNIT_TO_CALCULATE;
        Price = price;
        Number = number;

    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getSerial() {
        return Serial;
    }

    public void setSerial(String serial) {
        Serial = serial;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getKind() {
        return Kind;
    }

    public void setKind(String kind) {
        Kind = kind;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getAvatar() {
        return Avatar;
    }

    public void setAvatar(String avatar) {
        Avatar = avatar;
    }

    public String getUNIT_TO_CALCULATE() {
        return UNIT_TO_CALCULATE;
    }

    public void setUNIT_TO_CALCULATE(String UNIT_TO_CALCULATE) {
        this.UNIT_TO_CALCULATE = UNIT_TO_CALCULATE;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public int getNumber() {
        return Number;
    }

    public void setNumber(int number) {
        Number = number;
    }
}

