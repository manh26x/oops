package com.tanlap.admin.controller;

import com.tanlap.admin.security.services.BaseServices.FileStorageServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/product/file")
public class FileController {
  @Autowired
    FileStorageServices fileStorageServices;
    @PreAuthorize("hasRole('USER')")
    @PostMapping(value = "/uploads/{file}")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file){
        String mess="";
        try {
            fileStorageServices.save(file);
            mess=" đã tải lên file "+file.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.OK).body(mess);
        } catch (Exception e) {
            mess=" Không thể tải lên file "+file.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(mess);
        }
    }
}
