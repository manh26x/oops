package com.tanlap.admin.controller;

import com.tanlap.admin.payload.response.MessageResponse;
import com.tanlap.admin.security.services.BaseServices.FileStorageServices;
import com.tanlap.admin.security.services.ProductImp;
import com.tanlap.admin.security.services.ProductServiceIm;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/product")
public class MainProductController {
    @Autowired
    ProductImp productImp;
    @Autowired
    ProductServiceIm productServiceIm;


    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @GetMapping("/gach-truyen-thong")
    public List<ProductImp> ListofTraBrick(){
        return productServiceIm.filterByKind("gach-truyen-thong");
    }


    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @GetMapping("/gach-op-lat")
    public List<ProductImp> ListofTiledBrick()
    {
        return productServiceIm.filterByKind("gach-op-lat");
    }


    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @GetMapping("/be-tong-tuoi")
    public List<ProductImp> ListofConcre(){
        return productServiceIm.filterByKind("be tong tuoi");
    }


    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @GetMapping("/da-veneer")
    public List<ProductImp>ListofVeneer(){
        return productServiceIm.filterByKind("da-veneer");
    }


    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @GetMapping("/da-granite")
    public List<ProductImp> ListofGranite(){
        return productServiceIm.filterByKind("da-granite");
    }


    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @GetMapping("/da-cam-thach")
    public List<ProductImp> ListofJade(){
        return productServiceIm.filterByKind("da-cam-thach");
    }


    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @GetMapping("/all")
    public List<ProductImp> ListofAll(){
        return productServiceIm.findAll();
    }


    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @GetMapping(value = "/find/name/{Name}")
    public ResponseEntity<?> findName(@PathVariable("Name") String name){
        var x= productServiceIm.findByName(name);
        return !x.isPresent() ? ResponseEntity.badRequest().body(new MessageResponse("Cannot find the name of product")): ResponseEntity.ok(x.get());
    }


    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @GetMapping(value = "/find/price/{Price}")
    ResponseEntity<?> findByPrice(@PathVariable("Price") double price){
        var data= productServiceIm.findByPrice(price);
        return data==null? ResponseEntity.badRequest().body(new MessageResponse("Cannot find any product equal the price")):
                ResponseEntity.ok(data);
    }



/*
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @PostMapping(@Param()"/find/seri/")
    public ResponseEntity<?> findSeri(String seri){
        var x=productRepository.findBySerial(seri);
        return !x.isPresent() ? ResponseEntity.badRequest().body(new MessageResponse("Cannot find the serial line of product")): ResponseEntity.ok(x.get());
    }
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @PostMapping("/find/ngay/{msd}")
    public List<Product> listMSDtoProduct(String msd){
        DateFormat df= new SimpleDateFormat("mm/dd/yyyy");
        Date x=null;
        try {
           x =df.parse(msd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return productRepository.filterByMSD(x);
    }
*/


}
