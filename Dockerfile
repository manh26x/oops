FROM openjdk:8
ADD tanlap.jar tanlap.jar
EXPOSE 8099
ENTRYPOINT ["java", "-jar", "tanlap.jar"]
